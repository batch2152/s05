@extends('layouts.app')

@section('content')

    <h1>Tasks</h1>


        <h1 class="text-center">To Do</h1>
            <div class="text-center mt-5">
                <form class="row g-3 justify-content-center" method="post" action="/tasks">
                @csrf
            <div class="col-6">
                <input type="text" class="form-control" name="task" placeholder="Add Task">                       
                <input type="hidden" name="status" value="pending">
            </div>
            <div class="col-auto">
                <button type="submit" class="btn btn-success">Submit</button>
            </div>
                </form>
            </div>

            <div class="text-center">
            <h2>All Todos</h2>
                <div class="row justify-content-center">
                    <div class="col-lg-6">

                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Name</th>
                                <th scope="col">Status</th>
                                <th scope="col">Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if(count($tasks) > 0)
                                @foreach($tasks as $task)
                                    <tr>
                                        <th>{{ $task->id }}</th>
                                        <td>{{ $task->title }}</td>
                                        <td>{{ $task->body }}</td>
                                        @if(!Auth::guest())
	                        	        @if(Auth::user()->id === $task->user_id)
                                        
                                            <td>
                                            
                                                <a href="/tasks/{{$task->id}}/edit" class="btn btn-info" type="button">Edit</a>
                                                <a href="{{route('tasks.destory',['task'=>$task->id])}}" class="btn btn-danger">Delete</a>
                                            </td>
                                        @endif
		                                @endif     
                                    </tr>

                                @endforeach
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>











@endsection
@extends('layouts.app')

@section('content')

<h1>Edit Task</h1>
    <form method="post" action="/tasks/{{$task->id}}">
        @method('PUT')
        @csrf
        <div class="form-group">
                <label for="title">Name</label>
                <input class="form-control" placeholder="name" name="name" type="text" id="name" value="{{ $task->title}}">
        </div>

        <div class="form-group">
                <label for="title">Status</label>
                <input class="form-control" placeholder="status" name="status" rows="10" id="status"value="{{ $task->body }}">
        </div>
        <br>
        <button class="btn btn-primary" type="submit">Update</button>
        <a href="/tasks" class="btn btn-secondary" type="button">Go Back</a>
    </form>

















@endsection